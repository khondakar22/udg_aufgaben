/**
 * @author Khondkar Readul Islam
 * @description UDG Aufgaben
 * @version 0.0.1v
 * @summary Using State Pattern to change the navigation and Module pattern for handling the other DOM Manipulation. Insteadt of using Class I used prototype based OOP. Some features of ES6 I have implemented to give flavour of the next Generation JavaScript. For Example use of const and let
 */

/** State Patter Start */

// For Dynamic Content
const NavState = function() {
  let currentState = new dashboardState(this);

  this.init = function() {
    this.changeState(new dashboardState());
  };

  this.changeState = function(state) {
    currentState = state;
  };
};

// Dashboard state
const dashboardState = function() {
  document.querySelector("#main-content").innerHTML = `
  <section id="upload-card">
            <h1>Upload Document</h1>
            <!-- Upload  Grid Container start -->
            <div id="upload-card--item" class="container">
              <div class="row">
              <!-- Upload Card Start -->
              <div class="col-sm-12">  
                  <div class="card">
                      <div class="card-body">
                          <form id="upload-form-data" >
                              <div class="form-group">
                                <label for="csv_file">File Input</label>
                                <input type="file" class="btn btn-dark form-control-file" id="csv_file" name="csv_file" accept=".csv">
                              </div>
                              <button type="submit" class="btn btn-primary btn-block" id="upload-button">Upload</button>
                            </form>
                      </div>
                    </div>
              </div>
               <!-- Upload Card Card End -->
              </div>
            </div>
            <!-- Upload  Grid Container End -->
        </section>
        <section id="table-chart-card">
          <!-- Table&Chart Grid Container Start -->
          <div class="container">
            <div class="row">
               <!-- Chart Card Start -->
              <div class="col-lg-5">  
                    <div class="card">
                        <canvas id="myChart" class="card-img-top"  width="400" height="500"></canvas>
                        <div class="card-body">
                          <h5 class="card-title">Product Type</h5>
                          <p class="card-text">This is the quick Statistics of your product type that you has uploaded </p>
                          <a href="#" class="btn btn-primary" id="change-chart">Change Chart</a>
                        </div>
                      </div>
              </div>
               <!-- Chart Card End -->
              <!-- ----------------------------- -->
              <!-- Table Card Start -->
              <div class="col-lg-7">
                 <div class="card">
                   <div class="card-body">
                      <button type="button" class="btn btn-primary btn-lg btn-block" id="createChart" disabled>Visualize the Result</button>
                      <hr>
                      <!-- Table -->
                      <table id="example" class="table table-striped table-bordered dt-responsive" style="width:100%">
                          <thead> 
                             <tr id="th">
                               <th></th>
                             </tr>
                          </thead>
                          <tbody id="tb">
                              <tr>
                                  <td></td>
                              </tr> 
                          </tbody> 
                      </table>
                   </div>
                 </div>
              </div>
              </div>
              <!-- Table Card End -->
            </div>
          </div>
          <!-- Table&Chart Grid Container Container End -->
        </section>
  `;
};
// Documentation page state
const documentationState = function() {
  document.querySelector("#main-content").innerHTML = `
<section> 
<div class="list-group">
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
  
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Tool Used</h5>
        </div>
        <ul> 
              <li> HTML5</li>
              <li> CSS3 </li>
              <li> Vanilla JavaScript</li>
              <li> BootStrap 4</li>
              <li> Chart.js </li>
              <li> Jquery DataTable Plugin</li>
        </ul>
  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Responsive Design With Core CSS3 for Main Wireframe</h5>
            <small class="text-muted">100%</small>
    </div>
    <div class="progress">
            <div class="progress-bar  bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%
            </div>
    </div>
  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">CSV Import with PHP does not work</h5>
              <small class="text-muted">0%</small>
    </div>
    <div class="progress">
            <div class="progress-bar " role="progressbar" style="width: 0%;color:black;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%
    </div>
    </div>
  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
      <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">CSV Import with Vanilla JavaScript</h5>
            <small class="text-muted">70%</small>
      </div>
      <div class="progress">
          <div class="progress-bar bg-warning" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">70%
          </div>
      </div>
  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
      <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">For CSV Data Rendering used 3rd Party Library Jquery DataTable Plugin. Becuase It has strong functionaly to show the data table in a responsive way to save time. But Unfortunately, after importing the big csv, responsiveness is broken down. </h5>
            <small class="text-muted">50%</small>
      </div>
      <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" style="width: 50%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">50%
            </div>
      </div>
  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
      <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Chart.js was used for data visualization. It's very easy to use. I have used to different chart for the same data </h5>
            <small class="text-muted">90%</small>
      </div>
      <div class="progress">
          <div class="progress-bar bg-primary" role="progressbar" style="width: 90%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">90%
          </div>
      </div>
  </a>
  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Table Export to CSV, Excel and PDF are added. I used Jquery Data Table as well as. But doesn't work properly  </h5>
            <small class="text-muted">10%</small>
        </div>
        <div class="progress">
            <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">10%
            </div>
        </div>
  </a>
</div>
  </section>
  `;
};
// About me page state
const aboutMePageSate = function() {
  document.querySelector("#main-content").innerHTML = `
  <section>
  <div class="jumbotron">
  <h1 class="display-4">Hello, world!</h1>
  <p class="lead">I love to prove myself in any situation. Hardworking is the quote of life and efficiency is the practice of life. The combination of both lives will become smooth, so that, trying to implement every sector in my life. </p>
  <hr class="my-4">
  <p>If you want to say 'Hi', please visit my social pages</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" target= "_blank" href="https://www.linkedin.com/in/khondakar-readul-islam-565797163/" role="button">Linked in</a>
    <a class="btn btn-success btn-lg"  target= "_blank" href="https://www.xing.com/profile/KhondakarReadul_Islam/cv?sc_o=mxb_p" role="button">Xing</a>
    <a class="btn btn-info btn-lg"  target= "_blank" href="https://www.facebook.com/riyadislam786" role="button">Facebook</a>
  </p>
</div>
</section>
  `;
};

// Instantiate NavState
const navState = new NavState();

//Initailize the dashboard or first state
navState.init();

// Select Nav variable
const dashboard = document.getElementById("dashboardState"),
  documentation = document.getElementById("doc-nav"),
  aboutMe = document.getElementById("about-me");

// Event Listener for  dashBoard state
dashboard.addEventListener("click", e => {
  navState.changeState(new dashboard());
  e.preventDefault();
});

// Event Listener for documentation state
documentation.addEventListener("click", e => {
  navState.changeState(new documentationState());
  e.preventDefault();
});

// Event Listener for About me page state
aboutMe.addEventListener("click", e => {
  navState.changeState(new aboutMePageSate());
  e.preventDefault();
});
/** State Pattern End */

/********************************************************************************
 Without Any Pattern, by using Vanila JavaScript to read the csv file
 ********************************************************************************/
// FileReader use to read the csv file
document
  .querySelector("#upload-form-data")
  .addEventListener("submit", readFile);
// Read File Function()
function readFile(e) {
  document.getElementById("createChart").disabled = false;
  let file = document.getElementById("csv_file").files[0];
  let reader = new FileReader();
  reader.onload = function(file) {
    var file_data = file.target.result.split(/[\r?\n|\n]+/g);
    var table_data = "";
    var tbody = "";
    for (var count = 0; count < file_data.length; count++) {
      let cell_data = file_data[count].includes(";")
        ? file_data[count].split(";")
        : file_data[count].split(",");
      tbody += "<tr>";
      for (var cell_count = 0; cell_count < cell_data.length; cell_count++) {
        if (count === 0) {
          table_data += "<th>" + cell_data[cell_count] + "</th>";
          document.getElementById("th").innerHTML = table_data;
        } else {
          /**Below logic was tried to escape the extra line break, but unfortunately failed 
          if (
            cell_data[cell_count].charAt(0) === '"' &&
            cell_data[cell_count].charAt(
              cell_data[cell_count].length - 1
            ) !== '"'
          ) {
            var strcon = cell_data[cell_count].concat(
              cell_data[cell_count + 1]
            );

            table_data += "<td>" + strcon + "</td>";
            count++;
          } else {
            table_data += "<td>" + cell_data[cell_count] + "</td>";
          }
          ********************************************************************************* */
          tbody += "<td>" + cell_data[cell_count] + "</td>";
        }
      }
      tbody += "</tr>";
    }

    document.getElementById("tb").innerHTML = tbody;
  };
  clearInputField();
  reader.readAsText(file);
  e.preventDefault();
}

// Clear Input Field Function
function clearInputField(){
  document.getElementById('csv_file').value = '';
}

// Select the chart Element to render the chart
document.getElementById("createChart").addEventListener("click", drawChart);
// Change Chart Type
document.getElementById("change-chart").addEventListener("click", drawChart);

function drawChart(e) {
  // For Changing the chart Type
  let chartType;
  if (e.target.id === "createChart") {
    chartType = "pie";
  } else if (e.target.id === "change-chart") {
    chartType = "bar";
  }
  // Select the table data and define and get labels variable count listd for chart
  const tb = document.getElementById("tb").innerText,
    tShirtsCount = (tb.match(/T-Shirts/gi) || []).length,
    poloshirtsCount = (tb.match(/Poloshirts/gi) || []).length,
    taschenCount = (tb.match(/Taschen/gi) || []).length,
    pulloverCount = (tb.match(/Pullover/gi) || []).length,
    sweatjackenCount = (tb.match(/Sweatjacken/gi) || []).length,
    jogginghosenCount = (tb.match(/Jogginghosen/gi) || []).length,
    shortsCount = (tb.match(/Shorts/gi) || []).length,
    westenCount = (tb.match(/Westen/gi) || []).length,
    jackenCount = (tb.match(/Jacken/gi) || []).length,
    blusenCount = (tb.match(/Blusen/gi) || []).length,
    hemdenCount = (tb.match(/Hemden/gi) || []).length;

  // For Initialize the Chart
  const ctx = document.getElementById("myChart").getContext("2d");
  const myChart = new Chart(ctx, {
    type: chartType,
    data: {
      labels: [
        "T-Shirts",
        "Poloshirts",
        "Taschen",
        "Pullover",
        "Sweatjacken",
        "Jogginghosen",
        "Shorts",
        "Westen",
        "Jacken",
        "Blusen",
        "Hemden"
      ],
      datasets: [
        {
          label: "# of Products",
          data: [
            tShirtsCount,
            poloshirtsCount,
            taschenCount,
            pulloverCount,
            sweatjackenCount,
            jogginghosenCount,
            shortsCount,
            westenCount,
            jackenCount,
            blusenCount,
            hemdenCount
          ],
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(100,0,0,0.2)",
            "rgba(255,255,0,0.5)",
            "rgba(0,255,0,0.5)",
            "rgba(255,0,0,0.5)",
            "rgba(0,0,0,0.5)"
          ],
          borderColor: [
            "rgba(255,99,132,1)",
            "rgba(54, 162, 235, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
            "rgba(255, 159, 64, 1)",
            "rgba(100,0,0,1)",
            "rgba(255,255,0,1)",
            "rgba(0,255,0,1)",
            "rgba(255,0,0,1)",
            "rgba(0,0,0,1)"
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  });

  e.preventDefault();
}

// To edit the table
document.querySelector("table").addEventListener("click", function(e) {
  e.target.innerHTML = `<input type="text" id="innerHtml" value=${
    e.target.innerText
  }>`;
});

/********************************************************************************
 Above are without any Pattern, Vanila JavaScript
 ********************************************************************************/

/** Module Pattern Start */
const UserInterface = (function() {
  return {
    activateSideMenuBar: function() {
      // Select toggle button, backdrop class, mobile-nav class from HTML
      const toggleButton = document.querySelector(".toggle-button");
      const mobileNav = document.querySelector(".mobile-nav");
      const backdrop = document.querySelector(".backdrop");

      // Event Listener for Backdrop // ES5
      backdrop.addEventListener("click", function() {
        mobileNav.style.display = "none";
        closeSidebar();
      });
      // // Close Sidebar
      function closeSidebar() {
        backdrop.style.display = "none";
      }
      // Event Listeners for ToggleButton //ES6 (Fat Arrow Function)
      toggleButton.addEventListener("click", e => {
        mobileNav.style.display = "block";
        backdrop.style.display = "block";
        // e.preventDefault();
      });
    }
  };
})();

const AppController = (function(UserInterface) {
  //Public Method
  return {
    init: function() {
      UserInterface.activateSideMenuBar();
    }
  };
})(UserInterface);

// Initialize APP Contoller
AppController.init();

/** Module Pattern End */
