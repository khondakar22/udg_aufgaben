

<?php

if(!empty($_FILES['csv_file']['name']))
{
 $file_data = fopen($_FILES['csv_file']['name'], 'r');
 fgetcsv($file_data);
 while(($row = fgetcsv($file_data, 1000, ';', '"', "\\") !== FALSE))
 {
  //  print_r($row);
  $data[] = array(
    'Hauptartikelnr'    => $row[0],
    'Artikelname'       => $row[1],
    'Hersteller'        => $row[2],
    'Beschreibung'      => $row[3],
    'Materialangaben'   => $row[4],
    'Geschlecht'        => $row[5],
    'Produktart'        => $row[6],
    'Ärmel'             => $row[7],
    'Bein'              => $row[8], 
    'Kragen'            => $row[9],
    'Herstellung'       => $row[10],
    'Taschenart'        => $row[11],
    'Grammatur'         => $row[12],
    'Material'          => $row[13],
    'Ursprungsland'     => $row[14],
    'Bildname'          => $row[15]
  );
  echo json_encode($data);
 }

}

?>