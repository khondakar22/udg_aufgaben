<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <!-- Bootstrap CSS -->
    <link
      rel="stylesheet"
      href=" https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"
    />
    <!-- DataTable Bootstrap CSS -->
    <link
      rel="stylesheet"
      href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"
    />
    <!-- Bootstrap Responsive CSS also for DataTable  -->
    <link
      rel="stylesheet"
      href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css"
    />
    <!-- Data Table Export Button CSS -->
    <link
      rel="stylesheet"
      href="http://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"
    />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assests/css/style.css" />

    <title>UDG | Aufgaben</title>
  </head>
  <body>
    <!-- Backdrop Start -->
    <div class="backdrop"></div>
    <!-- Backdrop End -->
    <!-- ----------------------------- -->
    <!-- Header section Start -->
    <header class="main-header">
      <div>
        <!-- Sidebar Toggle Button Start -->
        <button class="toggle-button">
          <span class="toggle-button__bar"></span>
          <span class="toggle-button__bar"></span>
          <span class="toggle-button__bar"></span>
        </button>
        <!-- Sidebar Toggle Button End -->
        <!-- ----------------------------- -->
        <!-- Logo Start -->
        <a href="index.html" id="dashboardState" class="main-header__logo">
          UDG CSV Project</a
        >
        <!-- Logo End -->
      </div>
      <!-- Desktop Navbar Start -->
      <nav class="main-nav">
        <ul class="main-nav__items">
          <li class="main-nav__item">
            <a href="#" id="doc-nav">Documentation</a>
          </li>
          <li class="main-nav__item"><a href="#" id="about-me">About me</a></li>
        </ul>
      </nav>
      <!-- Desktop Navbar End -->
    </header>
    <!-- Header Section End -->
    <!-- ----------------------------- -->
    <!-- Mobile Sidebar Start -->
    <nav class="mobile-nav">
      <ul class="mobile-nav__items">
        <li class="mobile-nav__item" ><a href="#" id="doc-nav">Documentation</a></li>
        <li class="mobile-nav__item" ><a href="#"  id="about-me">About me</a></li>
      </ul>
    </nav>
    <!-- Mobile Sidebar End -->
    <!-- ----------------------------- -->
    <!-- Main Section Start -->
    <main id="main-content"></main>
    <!-- Main Section End -->
    <!-- ----------------------------- -->
    <!-- Footer Section Start -->
    <footer class="main-footer">
      <ul class="main-footer__links">
        <li class="main-footer__link"><a href="#"> &copy; UDG 2018</a></li>
        <strong><b>|</b></strong>
        <li class="main-footer__link">
          <a href="#"> Developed By Khondakar Readul Islam</a>
        </li>
      </ul>
    </footer>
    <!-- Footer Section End -->
    <!-- ----------------------------- -->

    <!-- jQuery first, Data Table JS, Bootstrap JS, Responsive Bootstrap JS for Data Table -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="
https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <!-- Chart.js -->
    <script
      type="text/javascript"
      src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"
    ></script>
    <!-- Custom js -->
    <script src="assests/js/app.js"></script>
    <!-- Initialize the Data Table -->
    <script>
      $(document).ready(function() {
       const table =  $("#example")
          .DataTable({
            dom: "Bfrtip",
            buttons: [
              {
                extend: "excel",
                text: "Export to Excel",
                exportOptions: {
                  modifier: {
                    // DataTables core
                    order: "index", // 'current', 'applied', 'index',  'original'
                    page: "all", // 'all',     'current'
                    search: "none", // 'none',    'applied', 'removed'
                  }
                }
              },
              {
                extend: "csv",
                text: "Export to CSV",
                exportOptions: {
                  modifier: {
                    // DataTables core
                    order: "index", // 'current', 'applied', 'index',  'original'
                    page: "all", // 'all',     'current'
                    search: "none" // 'none',    'applied', 'removed'
                  }
                }
              },
              {
                extend: "pdf",
                text: "PDF",
                exportOptions: {
                  modifier: {
                    // DataTables core
                    order: "index", // 'current', 'applied', 'index',  'original'
                    page: "all", // 'all',     'current'
                    search: "none" // 'none',    'applied', 'removed'
                  }
                }
              },
            ],
            responsive: true
          }).responsive.recalc(); // Try to rebuild the responsiveness of data table
          table.buttons().container() // Add Bootstrap Button
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      });
    </script>
  </body>
</html>